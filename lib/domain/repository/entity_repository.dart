import 'package:meta/meta.dart';
import 'package:obliq/domain/model/entity.dart';

abstract class EntityRepository {
  Future<Entity> getMethod({@required int key, @required int num});
}
