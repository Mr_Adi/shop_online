import 'package:meta/meta.dart';

class Entity {
  final DateTime sunrise;
  final DateTime sunset;
  final DateTime solarNoon;
  final int dayLength;

  Entity({
    required this.sunrise,
    required this.sunset,
    required this.solarNoon,
    required this.dayLength,
  });
}